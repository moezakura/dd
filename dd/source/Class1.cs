using System;
using System.CodeDom.Compiler;
using System.Configuration;
using System.Runtime.CompilerServices;

namespace ns2
{
	[CompilerGenerated]
	[GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "14.0.0.0")]
	internal sealed class Class1 : ApplicationSettingsBase
	{
		private static Class1 class1_0;

		public static Class1 Class1_0
		{
			get
			{
				return Class1.class1_0;
			}
		}

		static Class1()
		{
			Class1.class1_0 = (Class1)SettingsBase.Synchronized(new Class1());
		}

		public Class1()
		{
		}
	}
}