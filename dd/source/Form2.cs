using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ns0
{
	public class Form2 : Form
	{
		private IContainer icontainer_0 = null;

		private WebBrowser webBrowser1;

		private Timer timer_0;

		public Form2()
		{
			this.InitializeComponent();
		}

		protected override void Dispose(bool disposing)
		{
			if ((!disposing ? false : this.icontainer_0 != null))
			{
				this.icontainer_0.Dispose();
			}
			base.Dispose(disposing);
		}

		private void Form2_Load(object sender, EventArgs e)
		{
		}

		private void InitializeComponent()
		{
			this.icontainer_0 = new System.ComponentModel.Container();
			this.webBrowser1 = new WebBrowser();
			this.timer_0 = new Timer(this.icontainer_0);
			base.SuspendLayout();
			this.webBrowser1.Dock = DockStyle.Fill;
			this.webBrowser1.Location = new Point(0, 0);
			this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
			this.webBrowser1.Name = "webBrowser1";
			this.webBrowser1.Size = new System.Drawing.Size(522, 319);
			this.webBrowser1.TabIndex = 0;
			this.webBrowser1.Url = new Uri("", UriKind.Relative);
			this.webBrowser1.Visible = false;
			this.timer_0.Interval = 12000;
			base.AutoScaleDimensions = new SizeF(6f, 12f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			base.ClientSize = new System.Drawing.Size(522, 319);
			base.Controls.Add(this.webBrowser1);
			base.Name = "Form2";
			this.Text = "Twitter Account Hack";
			base.Load += new EventHandler(this.Form2_Load);
			base.ResumeLayout(false);
		}
	}
}