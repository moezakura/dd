using ns4;
using System;
using System.Windows.Forms;

namespace ns3
{
	internal static class Class2
	{
		[STAThread]
		private static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new Form1());
		}
	}
}