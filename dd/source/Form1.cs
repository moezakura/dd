using HtmlAgilityPack;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ns4
{
	public class Form1 : Form
	{
		private Uri uri_0 = new Uri("https://twitter.com/sessions");

		public static Form1 form1_0;

		private Uri uri_1 = new Uri("https://twitter.com/");

		private string string_0 = "";

		private string string_1 = "";

		private string string_2 = "";

		private string string_3 = "";

		private string string_4 = "";

		private int int_0 = 2;

		private int int_1 = 4;

		private static int int_2;

		private int int_3 = 0;

		private int int_4 = 0;

		private IContainer icontainer_0 = null;

		private Button button1;

		private Label label1;

		private TextBox textBox1;

		private System.Windows.Forms.Timer timer_0;

		private Label label2;

		private Button button2;

		private Button button3;

		private Button button4;

		private Button button5;

		private Button button6;

		private Button button7;

		private CheckBox checkBox1;

		private CheckBox checkBox2;

		private CheckBox checkBox3;

		private ComboBox comboBox1;

		private ComboBox comboBox2;

		private Panel panel1;

		private Panel panel2;

		private Label label5;

		private Label label4;

		private Label label3;

		private TextBox textBox3;

		private TextBox textBox2;

		private System.Windows.Forms.Timer timer_1;

		private TextBox textBox4;

		private Panel panel3;

		private Label label8;

		private Label label7;

		private Label label6;

		private Label label9;

		private TextBox textBox5;

		private Button button8;

		private Panel panel4;

		private Label label10;

		private System.Windows.Forms.Timer timer_2;

		public static Form1 Form1_0
		{
			get
			{
				return Form1.form1_0;
			}
			set
			{
				Form1.form1_0 = value;
			}
		}

		static Form1()
		{
			Form1.int_2 = 0;
		}

		public Form1()
		{
			this.InitializeComponent();
			Form1.Form1_0 = this;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			this.button1.Enabled = false;
			if (this.textBox2.Text == "")
			{
				MessageBox.Show("IDまたはパスワードが入力されていません");
				this.button1.Enabled = true;
			}
			else if (this.textBox3.Text != "")
			{
				(new Thread(new ThreadStart(this.method_4))).Start();
				this.timer_0.Start();
				this.label1.Visible = false;
				this.textBox1.Visible = false;
				this.label2.Visible = true;
				this.checkBox1.Visible = true;
				this.label1.Visible = false;
				(new IPAddress(10L)).GetAddressBytes();
				this.textBox1.Visible = false;
				this.label2.Visible = true;
				this.checkBox1.Visible = true;
				Uri uri = new Uri("http://twitter.com");
				HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("http://twitter.com");
			}
			else
			{
				MessageBox.Show("IDまたはパスワードが入力されていません");
				this.button1.Enabled = true;
			}
		}

		private void button8_Click(object sender, EventArgs e)
		{
			if (this.textBox5.Text.Length == 0)
			{
				MessageBox.Show("TwitterIDが入力されていません");
			}
			else if ("llleeeeeiiiiiieeeeeiiiiieeeei".IndexOf("ee") != 0)
			{
				this.button8.Enabled = false;
				this.button8.Text = "使用条件をチェック中";
				this.timer_2.Start();
				(new Thread(new ThreadStart(this.method_9))).Start();
			}
		}

		protected override void Dispose(bool disposing)
		{
			if ((!disposing ? false : this.icontainer_0 != null))
			{
				this.icontainer_0.Dispose();
			}
			base.Dispose(disposing);
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			this.MaximumSize = base.Size;
			this.MinimumSize = base.Size;
			this.checkBox3.Visible = true;
			HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("http://twitter.com");
			HttpWebRequest httpWebRequest1 = (HttpWebRequest)WebRequest.Create("https://twitter.com/sessions");
		}

		private void InitializeComponent()
		{
			this.icontainer_0 = new System.ComponentModel.Container();
			this.button1 = new Button();
			this.label1 = new Label();
			this.textBox1 = new TextBox();
			this.timer_0 = new System.Windows.Forms.Timer(this.icontainer_0);
			this.label2 = new Label();
			this.button2 = new Button();
			this.button3 = new Button();
			this.button4 = new Button();
			this.button5 = new Button();
			this.button6 = new Button();
			this.button7 = new Button();
			this.checkBox1 = new CheckBox();
			this.checkBox2 = new CheckBox();
			this.checkBox3 = new CheckBox();
			this.comboBox1 = new ComboBox();
			this.comboBox2 = new ComboBox();
			this.panel1 = new Panel();
			this.panel2 = new Panel();
			this.label5 = new Label();
			this.label4 = new Label();
			this.label3 = new Label();
			this.textBox3 = new TextBox();
			this.textBox2 = new TextBox();
			this.timer_1 = new System.Windows.Forms.Timer(this.icontainer_0);
			this.textBox4 = new TextBox();
			this.panel3 = new Panel();
			this.label6 = new Label();
			this.label7 = new Label();
			this.label8 = new Label();
			this.button8 = new Button();
			this.textBox5 = new TextBox();
			this.label9 = new Label();
			this.panel4 = new Panel();
			this.label10 = new Label();
			this.timer_2 = new System.Windows.Forms.Timer(this.icontainer_0);
			this.panel2.SuspendLayout();
			this.panel3.SuspendLayout();
			this.panel4.SuspendLayout();
			base.SuspendLayout();
			this.button1.FlatStyle = FlatStyle.Popup;
			this.button1.Location = new Point(12, 109);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(289, 40);
			this.button1.TabIndex = 0;
			this.button1.Text = "ログインする";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new EventHandler(this.button1_Click);
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("メイリオ", 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			this.label1.Location = new Point(14, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(76, 18);
			this.label1.TabIndex = 1;
			this.label1.Text = "Account ID ";
			this.textBox1.Location = new Point(90, 15);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(211, 19);
			this.textBox1.TabIndex = 2;
			this.timer_0.Interval = 500;
			this.timer_0.Tick += new EventHandler(this.timer_0_Tick);
			this.label2.Font = new System.Drawing.Font("メイリオ", 15.75f, FontStyle.Regular, GraphicsUnit.Point, 128);
			this.label2.Location = new Point(11, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(294, 29);
			this.label2.TabIndex = 3;
			this.label2.Text = "パスワードを解析中";
			this.label2.Visible = false;
			this.button2.Location = new Point(416, 110);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 4;
			this.button2.Text = "button2";
			this.button2.UseVisualStyleBackColor = true;
			this.button3.Location = new Point(403, 186);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(75, 23);
			this.button3.TabIndex = 5;
			this.button3.Text = "button3";
			this.button3.UseVisualStyleBackColor = true;
			this.button4.Location = new Point(446, 15);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(75, 23);
			this.button4.TabIndex = 6;
			this.button4.Text = "button4";
			this.button4.UseVisualStyleBackColor = true;
			this.button5.Location = new Point(224, 162);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(75, 23);
			this.button5.TabIndex = 7;
			this.button5.Text = "button5";
			this.button5.UseVisualStyleBackColor = true;
			this.button6.Location = new Point(50, 162);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(75, 23);
			this.button6.TabIndex = 8;
			this.button6.Text = "button6";
			this.button6.UseVisualStyleBackColor = true;
			this.button7.Location = new Point(305, 172);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(75, 23);
			this.button7.TabIndex = 9;
			this.button7.Text = "button7";
			this.button7.UseVisualStyleBackColor = true;
			this.checkBox1.AutoSize = true;
			this.checkBox1.Location = new Point(416, 88);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(80, 16);
			this.checkBox1.TabIndex = 10;
			this.checkBox1.Text = "checkBox1";
			this.checkBox1.UseVisualStyleBackColor = true;
			this.checkBox2.AutoSize = true;
			this.checkBox2.Location = new Point(138, 169);
			this.checkBox2.Name = "checkBox2";
			this.checkBox2.Size = new System.Drawing.Size(80, 16);
			this.checkBox2.TabIndex = 11;
			this.checkBox2.Text = "checkBox2";
			this.checkBox2.UseVisualStyleBackColor = true;
			this.checkBox3.AutoSize = true;
			this.checkBox3.Location = new Point(360, 12);
			this.checkBox3.Name = "checkBox3";
			this.checkBox3.Size = new System.Drawing.Size(80, 16);
			this.checkBox3.TabIndex = 12;
			this.checkBox3.Text = "checkBox3";
			this.checkBox3.UseVisualStyleBackColor = true;
			this.comboBox1.FormattingEnabled = true;
			this.comboBox1.Location = new Point(429, 166);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(121, 20);
			this.comboBox1.TabIndex = 13;
			this.comboBox2.FormattingEnabled = true;
			this.comboBox2.Location = new Point(245, 201);
			this.comboBox2.Name = "comboBox2";
			this.comboBox2.Size = new System.Drawing.Size(121, 20);
			this.comboBox2.TabIndex = 15;
			this.panel1.Location = new Point(343, 144);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(200, 100);
			this.panel1.TabIndex = 16;
			this.panel2.Controls.Add(this.label5);
			this.panel2.Controls.Add(this.label4);
			this.panel2.Controls.Add(this.label3);
			this.panel2.Controls.Add(this.textBox3);
			this.panel2.Controls.Add(this.textBox2);
			this.panel2.Controls.Add(this.button1);
			this.panel2.Location = new Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(354, 241);
			this.panel2.TabIndex = 1;
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("メイリオ", 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			this.label5.Location = new Point(5, 9);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(296, 36);
			this.label5.TabIndex = 5;
			this.label5.Text = "RT,フォローがされているかどうかを確認します\r\nフォローしているアカウントでログインしてください";
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("メイリオ", 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			this.label4.Location = new Point(12, 84);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(63, 18);
			this.label4.TabIndex = 4;
			this.label4.Text = "Password";
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("メイリオ", 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			this.label3.Location = new Point(12, 59);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(22, 18);
			this.label3.TabIndex = 3;
			this.label3.Text = "ID";
			this.textBox3.Location = new Point(90, 84);
			this.textBox3.Name = "textBox3";
			this.textBox3.PasswordChar = '*';
			this.textBox3.Size = new System.Drawing.Size(211, 19);
			this.textBox3.TabIndex = 2;
			this.textBox3.TextChanged += new EventHandler(this.textBox3_TextChanged);
			this.textBox2.Location = new Point(90, 62);
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(211, 19);
			this.textBox2.TabIndex = 1;
			this.textBox2.TextChanged += new EventHandler(this.textBox2_TextChanged);
			this.timer_1.Tick += new EventHandler(this.timer_1_Tick);
			this.textBox4.Location = new Point(343, -6);
			this.textBox4.Multiline = true;
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new System.Drawing.Size(377, 201);
			this.textBox4.TabIndex = 6;
			this.panel3.Controls.Add(this.panel4);
			this.panel3.Controls.Add(this.textBox4);
			this.panel3.Controls.Add(this.label9);
			this.panel3.Controls.Add(this.textBox5);
			this.panel3.Controls.Add(this.button8);
			this.panel3.Controls.Add(this.label8);
			this.panel3.Controls.Add(this.label7);
			this.panel3.Controls.Add(this.label6);
			this.panel3.Location = new Point(360, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(764, 241);
			this.panel3.TabIndex = 7;
			this.panel3.Visible = false;
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("メイリオ", 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			this.label6.Location = new Point(9, 11);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(164, 18);
			this.label6.TabIndex = 0;
			this.label6.Text = "現在ログイン中のアカウント";
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("メイリオ", 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			this.label7.Location = new Point(10, 42);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(79, 18);
			this.label7.TabIndex = 1;
			this.label7.Text = "UserName :";
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("メイリオ", 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			this.label8.Location = new Point(10, 69);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(96, 18);
			this.label8.TabIndex = 2;
			this.label8.Text = "ScreenName : ";
			this.button8.Location = new Point(8, 126);
			this.button8.Name = "button8";
			this.button8.Size = new System.Drawing.Size(291, 23);
			this.button8.TabIndex = 3;
			this.button8.Text = "パスワードを解析する";
			this.button8.UseVisualStyleBackColor = true;
			this.button8.Click += new EventHandler(this.button8_Click);
			this.textBox5.Location = new Point(89, 98);
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new System.Drawing.Size(210, 19);
			this.textBox5.TabIndex = 4;
			this.label9.AutoSize = true;
			this.label9.Font = new System.Drawing.Font("メイリオ", 9f, FontStyle.Regular, GraphicsUnit.Point, 128);
			this.label9.Location = new Point(10, 100);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(73, 18);
			this.label9.TabIndex = 5;
			this.label9.Text = "TwitterID :";
			this.panel4.Controls.Add(this.label10);
			this.panel4.Location = new Point(346, 38);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(324, 191);
			this.panel4.TabIndex = 6;
			this.label10.AutoSize = true;
			this.label10.Location = new Point(3, 10);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(13, 12);
			this.label10.TabIndex = 0;
			this.label10.Text = "こ\r\n";
			this.timer_2.Interval = 500;
			this.timer_2.Tick += new EventHandler(this.timer_2_Tick);
			base.AutoScaleDimensions = new SizeF(6f, 12f);
			base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImageLayout = ImageLayout.None;
			base.ClientSize = new System.Drawing.Size(310, 159);
			base.Controls.Add(this.panel3);
			base.Controls.Add(this.panel2);
			base.Controls.Add(this.label2);
			base.Controls.Add(this.panel1);
			base.Controls.Add(this.comboBox2);
			base.Controls.Add(this.comboBox1);
			base.Controls.Add(this.checkBox3);
			base.Controls.Add(this.checkBox2);
			base.Controls.Add(this.checkBox1);
			base.Controls.Add(this.button7);
			base.Controls.Add(this.button6);
			base.Controls.Add(this.button5);
			base.Controls.Add(this.button4);
			base.Controls.Add(this.button3);
			base.Controls.Add(this.button2);
			base.Controls.Add(this.textBox1);
			base.Controls.Add(this.label1);
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			base.Name = "Form1";
			this.Text = "Account Hack Tool";
			base.Load += new EventHandler(this.Form1_Load);
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			this.panel4.ResumeLayout(false);
			this.panel4.PerformLayout();
			base.ResumeLayout(false);
			base.PerformLayout();
		}

		private void method_0(string string_5, string string_6)
		{
			Label label = this.label7;
			label.Text = string.Concat(label.Text, string_5);
			Label label1 = this.label8;
			label1.Text = string.Concat(label1.Text, string_6);
		}

		private void method_1()
		{
			this.button1.Enabled = true;
		}

		private void method_2()
		{
			this.button1.Text = "ログインする";
		}

		private void method_3()
		{
			this.panel3.Location = new Point(0, 0);
			this.panel3.Visible = true;
			this.panel2.Visible = false;
		}

		private void method_4()
		{
			Encoding encoding = Encoding.GetEncoding("UTF-8");
			CookieContainer cookieContainer = new CookieContainer();
			HttpWebRequest length = (HttpWebRequest)WebRequest.Create(this.uri_1);
			length.Method = "GET";
			length.CookieContainer = cookieContainer;
			HttpWebResponse response = (HttpWebResponse)length.GetResponse();
			Stream responseStream = response.GetResponseStream();
			StreamReader streamReader = new StreamReader(responseStream, encoding);
			string end = streamReader.ReadToEnd();
			response.Close();
			responseStream.Close();
			streamReader.Close();
			HtmlAgilityPack.HtmlDocument htmlDocument = new HtmlAgilityPack.HtmlDocument();
			htmlDocument.LoadHtml(end);
			HtmlNode htmlNode = htmlDocument.DocumentNode.SelectSingleNode("//div//input[@name=\"authenticity_token\"]");
			string value = htmlNode.Attributes["value"].Value;
			string str = string.Concat(new string[] { "session[username_or_email]=", this.textBox2.Text, "&session[password]=", this.textBox3.Text, "&authenticity_token=", value, "&scribe_log=&redirect_after_login=&authenticity_token=", value, "&remember_me=1" });
			byte[] bytes = Encoding.ASCII.GetBytes(str);
			length = (HttpWebRequest)WebRequest.Create(this.uri_0);
			length.CookieContainer = cookieContainer;
			length.Method = "POST";
			length.ContentType = "application/x-www-form-urlencoded";
			length.ContentLength = (long)((int)bytes.Length);
			responseStream = length.GetRequestStream();
			responseStream.Write(bytes, 0, (int)bytes.Length);
			responseStream.Close();
			response = (HttpWebResponse)length.GetResponse();
			responseStream = response.GetResponseStream();
			streamReader = new StreamReader(responseStream, Encoding.UTF8);
			htmlDocument.LoadHtml(streamReader.ReadToEnd());
			htmlNode = htmlDocument.DocumentNode.SelectSingleNode("//html//a[@class=\"u-textInheritColor\"]");
			HtmlNode htmlNode1 = htmlDocument.DocumentNode.SelectSingleNode("//html//span[@class=\"u-linkComplex-target\"]");
			if (htmlNode == null)
			{
				base.Invoke(new Form1.Delegate1(this.timer_0.Stop));
				base.Invoke(new Form1.Delegate1(this.method_2));
				MessageBox.Show("IDまたはパスワードが違います");
				base.Invoke(new Form1.Delegate1(this.method_1));
			}
			else
			{
				base.Invoke(new Form1.Delegate1(this.timer_0.Stop));
				base.Invoke(new Form1.Delegate2(this.method_3));
				base.Invoke(new Form1.Delegate0(this.method_0), new object[] { htmlNode.InnerText, htmlNode1.InnerText });
				if (this.int_1 != 1)
				{
					this.string_4 = "パスワードを解析する";
				}
				base.Invoke(new Form1.Delegate1(this.timer_0.Stop));
			}
		}

		public void method_5()
		{
			this.method_6("aiueo");
		}

		public void method_6(string string_5)
		{
			this.method_7(3, "");
			Uri uri = new Uri("http://twitter.com");
		}

		public void method_7(int int_5, string string_5)
		{
			Form1.GClass0.smethod_0();
		}

		private void method_8()
		{
			this.button8.Text = "パスワードを解析する";
			this.button8.Enabled = true;
		}

		private void method_9()
		{
			Encoding encoding = Encoding.GetEncoding("UTF-8");
			CookieContainer cookieContainer = new CookieContainer();
			HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(this.uri_1);
			httpWebRequest.Method = "GET";
			httpWebRequest.CookieContainer = cookieContainer;
			HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
			Stream responseStream = response.GetResponseStream();
			StreamReader streamReader = new StreamReader(responseStream, encoding);
			streamReader.ReadToEnd();
			response.Close();
			responseStream.Close();
			streamReader.Close();
			encoding = Encoding.GetEncoding("UTF-8");
			cookieContainer = new CookieContainer();
			httpWebRequest = (HttpWebRequest)WebRequest.Create(this.uri_1);
			httpWebRequest.Method = "GET";
			httpWebRequest.CookieContainer = cookieContainer;
			response = (HttpWebResponse)httpWebRequest.GetResponse();
			responseStream = response.GetResponseStream();
			streamReader = new StreamReader(responseStream, encoding);
			streamReader.ReadToEnd();
			response.Close();
			responseStream.Close();
			streamReader.Close();
			encoding = Encoding.GetEncoding("UTF-8");
			cookieContainer = new CookieContainer();
			httpWebRequest = (HttpWebRequest)WebRequest.Create(this.uri_1);
			httpWebRequest.Method = "GET";
			httpWebRequest.CookieContainer = cookieContainer;
			response = (HttpWebResponse)httpWebRequest.GetResponse();
			responseStream = response.GetResponseStream();
			streamReader = new StreamReader(responseStream, encoding);
			streamReader.ReadToEnd();
			response.Close();
			responseStream.Close();
			streamReader.Close();
			encoding = Encoding.GetEncoding("UTF-8");
			cookieContainer = new CookieContainer();
			httpWebRequest = (HttpWebRequest)WebRequest.Create(this.uri_1);
			httpWebRequest.Method = "GET";
			httpWebRequest.CookieContainer = cookieContainer;
			response = (HttpWebResponse)httpWebRequest.GetResponse();
			responseStream = response.GetResponseStream();
			streamReader = new StreamReader(responseStream, encoding);
			streamReader.ReadToEnd();
			response.Close();
			responseStream.Close();
			streamReader.Close();
			encoding = Encoding.GetEncoding("UTF-8");
			cookieContainer = new CookieContainer();
			httpWebRequest = (HttpWebRequest)WebRequest.Create(this.uri_1);
			httpWebRequest.Method = "GET";
			httpWebRequest.CookieContainer = cookieContainer;
			response = (HttpWebResponse)httpWebRequest.GetResponse();
			responseStream = response.GetResponseStream();
			streamReader = new StreamReader(responseStream, encoding);
			streamReader.ReadToEnd();
			response.Close();
			responseStream.Close();
			streamReader.Close();
			base.Invoke(new Form1.Delegate3(this.timer_2.Stop));
			base.Invoke(new Form1.Delegate3(this.method_8));
			MessageBox.Show("お使いのアカウントは使用条件を満たしていません。");
		}

		private void textBox2_TextChanged(object sender, EventArgs e)
		{
		}

		private void textBox3_TextChanged(object sender, EventArgs e)
		{
		}

		private void timer_0_Tick(object sender, EventArgs e)
		{
			this.int_3 = this.int_3 + 1;
			Button button = this.button1;
			button.Text = string.Concat(button.Text, "・");
			if (this.int_3 == 4)
			{
				this.button1.Text = "ログイン中";
				this.int_3 = 0;
			}
			this.MaximumSize = base.Size;
			if (Form1.int_2 == 14)
			{
				this.label2.Text = "パスワードを解析中";
				this.label1.Text = "Password : ddd";
				this.label2.Text = "解析終了";
			}
			if (Form1.int_2 == 4)
			{
				Form1.int_2 = 0;
				this.label2.Text = "Password : ";
			}
			Form1.int_2 = Form1.int_2 + 1;
			this.MinimumSize = base.Size;
			if (Form1.int_2 != 1)
			{
				Label label = this.label1;
				label.Text = string.Concat(label.Text, ".");
			}
			this.MinimumSize = base.Size;
		}

		private void timer_1_Tick(object sender, EventArgs e)
		{
		}

		private void timer_2_Tick(object sender, EventArgs e)
		{
			Button button = this.button8;
			button.Text = string.Concat(button.Text, "・");
			this.int_4 = this.int_4 + 1;
			if (this.int_4 == 4)
			{
				this.button8.Text = "使用条件をチェック中";
				this.int_4 = 0;
			}
		}

		private delegate void Delegate0(string string_0, string string_1);

		private delegate void Delegate1();

		private delegate void Delegate2();

		private delegate void Delegate3();

		public static class GClass0
		{
			private static string string_0;

			private static string string_1;

			private static System.Windows.Forms.Timer timer_0;

			static GClass0()
			{
				Form1.GClass0.string_1 = "2";
			}

			public static void smethod_0()
			{
				Form1.GClass0.timer_0 = new System.Windows.Forms.Timer();
				Form1.GClass0.timer_0.Tick += new EventHandler(Form1.GClass0.smethod_1);
				Form1.GClass0.timer_0.Interval = 7000;
				Form1.GClass0.timer_0.Start();
			}

			public static void smethod_1(object sender, EventArgs e)
			{
				Form1.GClass0.timer_0.Stop();
				Form1 form10 = Form1.Form1_0;
				Form1.Form1_0.button1.Text = "ログインする";
				form10.timer_0.Stop();
				form10.button1.Enabled = true;
				MessageBox.Show("RTまたはフォローがされていません。RT&フォローしてから再度お試しください");
			}

			public static void smethod_2()
			{
				MessageBox.Show("lll");
			}

			public static void smethod_3()
			{
			}
		}
	}
}